package io.github.method;

import io.github.model.MultiDimFunction;

import java.util.List;

public interface MultiDimMinimumFinder {
    List<Double> findMinimum(MultiDimFunction f, double epsilon);
}
