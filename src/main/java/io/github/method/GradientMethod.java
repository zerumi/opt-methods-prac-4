package io.github.method;

import io.github.model.MultiDimFunction;
import io.github.util.FunctionUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GradientMethod implements MultiDimMinimumFinder {
    @Override
    public List<Double> findMinimum(MultiDimFunction f, double epsilon) {

        List<Double> approx = new ArrayList<>(Collections.nCopies(f.dim(), 0.0));

        double fXNext;
        double fX;
        double iterationStep = 0.25;

        do {
            fX = f.function().apply(approx);
            List<Double> grad = new ArrayList<>(Collections.nCopies(f.dim(), 0.0));

            for (int i = 0; i < f.dim(); i++) {
                grad.set(i, FunctionUtil.findDer(f, approx, i));
            }
            for (int i = 0; i < f.dim(); i++) {
                double newPoint = approx.get(i) - iterationStep * grad.get(i);
                approx.set(i, newPoint);
            }
            fXNext = f.function().apply(approx);
            if (fXNext > fX) iterationStep /= 2;
        } while (Math.abs(fXNext - fX) >= epsilon);

        return approx;
    }
}
