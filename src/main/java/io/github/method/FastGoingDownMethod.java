package io.github.method;

import io.github.model.MultiDimFunction;
import io.github.util.FunctionUtil;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

public class FastGoingDownMethod implements MultiDimMinimumFinder {

    @Override
    public List<Double> findMinimum(MultiDimFunction f, double epsilon) {

        List<Double> approx = new java.util.ArrayList<>(Collections.nCopies(f.dim(), 0.0));

        List<Double> grad = FunctionUtil.findGrad(f, approx);
        while (FunctionUtil.absVector(grad) >= epsilon) {
            double gradAbs = FunctionUtil.absVector(grad);
            List<Double> s = new java.util.ArrayList<>(Collections.nCopies(f.dim(), 0.0));
            for (int i = 0; i < f.dim(); i++) {
                s.set(i, grad.get(i) / gradAbs);
                int finalI = i;
                Function<BigDecimal, BigDecimal> func =
                        x -> BigDecimal.valueOf(approx.get(finalI)).add(x.multiply(BigDecimal.valueOf(s.get(finalI))));
                double lambda = new HalfDivisionMethod().findMinimum(
                        BigDecimal.valueOf(0),
                        BigDecimal.valueOf(10),
                        BigDecimal.valueOf(epsilon),
                        32,
                        func
                ).doubleValue();

                approx.set(i, approx.get(i) - lambda * s.get(i));
            }
            grad = FunctionUtil.findGrad(f, approx);
        }

        return approx;
    }
}
