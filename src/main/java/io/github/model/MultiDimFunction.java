package io.github.model;

import java.util.List;
import java.util.function.Function;

public record MultiDimFunction(int dim, Function<List<Double>, Double> function) {
}
