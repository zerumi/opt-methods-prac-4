package io.github;

import io.github.method.FastGoingDownMethod;
import io.github.method.GradientMethod;
import io.github.method.MultiDimMinimumFinder;
import io.github.model.MultiDimFunction;

import java.util.List;

public class Main {
    public static final MultiDimFunction f = new MultiDimFunction(
            2,
            doubles -> doubles.get(0) * doubles.get(0) - doubles.get(0) * doubles.get(1) +
                    doubles.get(1) * doubles.get(1) - 2 * doubles.get(0) + doubles.get(1)
    );

    public static void printMethodResult(MultiDimFunction f, MultiDimMinimumFinder method) {
        List<Double> result = method.findMinimum(f, 0.0005);
        result.forEach(System.out::println);
        System.out.println("Result: " + f.function().apply(result));
    }

    public static void main(String[] args) {
        System.out.println("---Gradient Method---");
        printMethodResult(f, new GradientMethod());
        System.out.println("---Fast going down method---");
        printMethodResult(f, new FastGoingDownMethod());
    }
}