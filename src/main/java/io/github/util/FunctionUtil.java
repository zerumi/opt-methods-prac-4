package io.github.util;

import io.github.model.MultiDimFunction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FunctionUtil {

    public static final double STEP = 0.005;

    public static double findDer(MultiDimFunction f, List<Double> point, int argument) {
        List<Double> step = new java.util.ArrayList<>(List.copyOf(point));
        step.set(argument, step.get(argument) + STEP);
        double fStep = f.function().apply(step);
        double fPoint = f.function().apply(point);
        return (fStep - fPoint) / STEP;
    }

    public static List<Double> findGrad(MultiDimFunction f, List<Double> point) {
        List<Double> grad = new ArrayList<>(Collections.nCopies(f.dim(), 0.0));

        for (int i = 0; i < f.dim(); i++) {
            grad.set(i, FunctionUtil.findDer(f, point, i));
        }

        return grad;
    }

    public static double absVector(List<Double> vector) {
        return Math.sqrt(vector.stream().mapToDouble(a -> a * a).sum());
    }
}
